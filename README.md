# Godoc-Docker

The target is to have an easy way to access all packaged files and documentation with a godoc container.

All what needed to be set is the `$GOPATH`. The `.env` file takes care to get it loaded properly for the compose file.

To run `godoc-docker`, you simply need to issue the following:

```bash
    docker-compose up -d
```

On the first time, the Container will be built.
Further starts will just use the built one.

By default, the local serving port is `10000`. Please adapt in the compose file, if needed.