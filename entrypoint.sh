#!/usr/bin/env bash

cp /godoc $GOPATH/bin/godoc

echo "Serving GoDoc. Check Exposed port."
godoc -http=:10000

