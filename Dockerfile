# Use already a golang
FROM golang:1.12.0-alpine3.9
LABEL maintainer=kay.haensge

RUN apk add git bash
RUN go get golang.org/x/tools/cmd/godoc
RUN cp $GOPATH/bin/godoc /godoc

ADD entrypoint.sh /entrypoint.sh
RUN chmod ugo+x /entrypoint.sh

ENTRYPOINT [ "/bin/bash", "-c", "/entrypoint.sh" ]




